package fm.ps.digident.views.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import fm.ps.digident.R
import fm.ps.digident.databinding.ActivityLoginBinding
import fm.ps.digident.databinding.ActivitySplashBinding
import fm.ps.digident.keys.Activities
import fm.ps.digident.validator.Validation
import kotlin.math.log

class LoginActivity : BaseActivity(), View.OnClickListener {


    //==============================================================================================
    // Binding ...
    var binding: ActivityLoginBinding? = null

    //==============================================================================================
    // On Create Activity ...
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        initializeActivity()
    }

    //==============================================================================================
    // Initialize Activity ...
    private fun initializeActivity(){
        listener()
    }

    //==============================================================================================
    // Listener Views In Activity ...
    private fun listener(){
        binding?.login?.setOnClickListener( this )
    }
    override fun onClick(view: View?) {
        when (view) {
            binding?.login -> {
                login()
            }
        }
    }

    //==============================================================================================
    // Login ...
    private fun login(){
        if (validationDataLogin()){
            startNewActivity( Activities.MAIN_ACTIVITY , null , true)
        }
    }

    private fun validationDataLogin() :Boolean{
        val validation = Validation()
        validation.addEmptyValidator(
            binding?.loginName,
            binding?.loginPassword
        )
        return validation.isValid
    }

}