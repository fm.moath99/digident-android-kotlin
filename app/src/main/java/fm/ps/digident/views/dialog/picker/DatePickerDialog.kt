package fm.ps.digident.views.dialog.picker

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputLayout
import java.util.*

class DatePickerDialog(private var textView: TextView?, private var  textInputLayout: TextInputLayout) : DialogFragment() , DatePickerDialog.OnDateSetListener {


    //==============================================================================================
    // OnCreate ...
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(requireActivity(), this, year, month, day)
    }

    //==============================================================================================
    // On Set Date ...
    @SuppressLint("SetTextI18n")
    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        val date = "$year.$month.$day"
        Toast.makeText(activity , date , Toast.LENGTH_LONG).show()
        if (textView == null){
            textInputLayout.editText!!.setText(date)
        }else if (textInputLayout == null){
            textView!!.text = date
        }
    }
}