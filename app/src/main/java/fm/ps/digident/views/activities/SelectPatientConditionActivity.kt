package fm.ps.digident.views.activities

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import fm.ps.digident.R
import fm.ps.digident.databinding.ActivitySelectPatientConditionBinding
import fm.ps.digident.keys.Activities

class SelectPatientConditionActivity : BaseActivity(), View.OnClickListener {

    //==============================================================================================
    // Binding ...
    var binding: ActivitySelectPatientConditionBinding? = null

    //==============================================================================================
    // On Create Activity ...
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_patient_condition)
        initializeActivity()
    }

    //==============================================================================================
    // Initialize Activity ...
    private fun initializeActivity(){
        listener()
    }

    //==============================================================================================
    // Listener Views In Activity ...
    private fun listener(){
        binding?.selectPatientNew?.setOnClickListener( this )
        binding?.selectPatientExists?.setOnClickListener( this )
    }
    override fun onClick(view: View?) {
        when (view) {
            binding?.selectPatientNew -> {
                startNewActivity( Activities.MEDICAL_HISTORY_ACTIVITY , null , false)
            }
            binding?.selectPatientExists -> {
                startNewActivity( Activities.LOGIN_ACTIVITY , null , false)
            }
        }
    }

}