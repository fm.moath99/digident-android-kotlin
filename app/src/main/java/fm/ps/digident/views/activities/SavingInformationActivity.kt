package fm.ps.digident.views.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import fm.ps.digident.R
import fm.ps.digident.databinding.ActivitySavingInformationBinding
import fm.ps.digident.keys.Activities
import fm.ps.digident.keys.Files
import fm.ps.digident.validator.Validation

import android.view.LayoutInflater

import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog


class SavingInformationActivity : BaseActivity() , View.OnClickListener{

    //==============================================================================================
    // Binding ...
    private var binding: ActivitySavingInformationBinding? = null

    //==============================================================================================
    // On Create Activity ...
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_saving_information)
        initializeActivity()
    }

    //==============================================================================================
    // Initialize Activity ...
    private fun initializeActivity(){
        binding?.savingInformationDate?.text = getCurrentDateTime(1)
        initializeSavingInformation()
        listener()
    }

    //==============================================================================================
    // Initialize Saving Information DSGVO ...
    @SuppressLint("SetJavaScriptEnabled")
    private fun initializeSavingInformation(){
        binding?.savingInformationWebView?.webViewClient = WebViewClient()
        binding?.savingInformationWebView?.settings?.cacheMode = WebSettings.LOAD_NO_CACHE
        binding?.savingInformationWebView?.loadUrl("file:///android_asset/SavingInformation.html")
        binding?.savingInformationWebView?.settings?.javaScriptEnabled = true
        binding?.savingInformationWebView?.settings?.setSupportZoom(true)

    }

    //==============================================================================================
    // Listener Views In Activity ...
    private fun listener(){
        binding?.btnNext?.setOnClickListener( this )
    }

    override fun onClick(view: View?) {
        when (view) {
            binding?.btnNext -> {
                nextActivity()
            }
        }
    }

    private fun nextActivity() {
        binding?.savingInformationDate?.text = getCurrentDateTime(1)
        if (isValid()){
            if (binding?.savingInformationSignaturePad?.isEmpty == false){
                hideView(binding?.btnNext!!)
                val content = binding?.content
                val screenShot = createScreenShot(content!!)
                saveScreenShot(screenShot!!)
                val pdfFile = createPdf(content , screenShot , Files.SAVING_INFORMATION)
                showMessage(pdfFile.toString())

                dialogFinishData()
                // Get the transferred data from source activity.
                showView(binding?.btnNext!!)
                startNewActivity( Activities.LOGIN_ACTIVITY , null , false)
            }else{
                showMessage("Bitte überprüfen Sie, ob Sie unterschrieben haben")
            }
        }
        else{
            showMessage( "Error" )
        }
    }

    //==============================================================================================
    // Additional ...
    private fun isValid(): Boolean {
        val validation = Validation()
        validation.addEmptyValidator(
            binding?.savingInformationSurName,
            binding?.savingInformationFirstName,
            binding?.savingInformationBirthDay,
            binding?.savingInformationAddress,
            binding?.savingInformationMobile,
            binding?.savingInformationLocation
        )
        return validation.isValid
    }

    private fun dialogFinishData(){
        val builder = AlertDialog.Builder(this)
        val viewGroup = findViewById<ViewGroup>(android.R.id.content)
        val dialogView: View =
            LayoutInflater.from(this ).inflate(R.layout.dialog_text, viewGroup, false)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        alertDialog.show()
    }

}