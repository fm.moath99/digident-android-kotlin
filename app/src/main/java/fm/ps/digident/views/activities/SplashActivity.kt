package fm.ps.digident.views.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.databinding.DataBindingUtil
import com.hanks.htextview.HTextView
import com.hanks.htextview.HTextViewType
import fm.ps.digident.R
import fm.ps.digident.databinding.ActivitySplashBinding
import fm.ps.digident.keys.Activities

@SuppressLint("CustomSplashScreen")
class SplashActivity : BaseActivity() {


    //==============================================================================================
    // Binding ...
    var binding: ActivitySplashBinding? = null

    //==============================================================================================
    // On Create Activity ...
    override fun onCreate(savedInstanceState: Bundle?) {
        hideStatusBar()
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        initializeActivity()
    }

    //==============================================================================================
    // Initialize Activity ...
    private fun initializeActivity(){
        initializeHtext()
        handlerActivity()
    }
    private fun initializeHtext(){
        val hTextView : HTextView = binding?.text!!

        hTextView.setAnimateType(HTextViewType.EVAPORATE)
        hTextView.animateText("zahnmedizinisches")

        Handler(Looper.getMainLooper()).postDelayed({
            hTextView.setAnimateType(HTextViewType.EVAPORATE)
            hTextView.animateText("versorgungszentrum")
            Handler(Looper.getMainLooper()).postDelayed({
                hTextView.setAnimateType(HTextViewType.EVAPORATE)
                hTextView.animateText("GmbH")

            } , 1000)

        } , 1000)
    }
    private fun handlerActivity(){
        Handler(Looper.getMainLooper()).postDelayed({
            startNewActivity( Activities.SELECT_PATIENT_CONDITION_ACTIVITY , null , true)
        } , 3000)
    }
}