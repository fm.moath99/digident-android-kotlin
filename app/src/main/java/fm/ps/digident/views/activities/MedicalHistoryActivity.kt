package fm.ps.digident.views.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import fm.ps.digident.R

import fm.ps.digident.databinding.ActivityMedicalHistoryBinding
import fm.ps.digident.keys.Activities
import fm.ps.digident.keys.Files
import fm.ps.digident.validator.Validation
import fm.ps.digident.views.dialog.picker.DatePickerDialog

class MedicalHistoryActivity : BaseActivity() ,View.OnClickListener {

    //==============================================================================================
    // Binding ...
    private var binding: ActivityMedicalHistoryBinding? = null

    //==============================================================================================
    // On Create Activity ...
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_medical_history)
        initializeActivity()
    }

    //==============================================================================================
    // Initialize Activity ...
    private fun initializeActivity(){
        binding?.medicalHistoryDate?.text = getCurrentDateTime(1)
        listener()
    }

    //==============================================================================================
    // Listener Views In Activity ...
    private fun listener(){
        binding?.btnNext?.setOnClickListener( this )
        binding?.etBirthdayLayout?.editText?.setOnClickListener( this )

        binding?.rbPregnancyNo?.setOnClickListener( this )
        binding?.rbPregnancyYes?.setOnClickListener( this )
        binding?.rbInfectiousDiseasesNo?.setOnClickListener( this )
        binding?.rbInfectiousDiseasesYes?.setOnClickListener( this )
        binding?.rbCardiovascularComplaintsNo?.setOnClickListener( this )
        binding?.rbCardiovascularComplaintsYes?.setOnClickListener( this )
        binding?.rbMetabolicDiseaseNo?.setOnClickListener( this )
        binding?.rbMetabolicDiseaseYes?.setOnClickListener( this )
        binding?.rbThyroidDiseasesNo?.setOnClickListener( this )
        binding?.rbThyroidDiseasesYes?.setOnClickListener( this )
        binding?.rbRenalOrLiverDiseasesNo?.setOnClickListener( this )
        binding?.rbRenalOrLiverDiseasesYes?.setOnClickListener( this )
        binding?.rbAllergiesNo?.setOnClickListener( this )
        binding?.rbAllergiesYes?.setOnClickListener( this )
        binding?.rbOtherDiseasesNo?.setOnClickListener( this )
        binding?.rbOtherDiseasesYes?.setOnClickListener( this )
        binding?.rbPreviousOperationsNo?.setOnClickListener( this )
        binding?.rbPreviousOperationsYes?.setOnClickListener( this )
        binding?.rbDrugsNo?.setOnClickListener( this )
        binding?.rbDrugsYes?.setOnClickListener( this )
    }

    override fun onClick(view: View?) {
        when (view) {
            binding?.btnNext -> {
                nextActivity()
            }
            binding?.etBirthdayLayout?.editText -> {
                val dateDialog = DatePickerDialog( null , binding?.etBirthdayLayout!!)
                dateDialog.show(supportFragmentManager, "datePicker")
            }
            binding?.rbPregnancyNo -> {
                hideView(binding?.etPregnancyMonthLayout!!)
                binding?.tvPregnancyQuestion?.error = null
            }
            binding?.rbPregnancyYes -> {
                showView(binding?.etPregnancyMonthLayout!!)
                binding?.tvPregnancyQuestion?.error = null
            }
            binding?.rbInfectiousDiseasesNo -> hideView(binding?.etInfectiousDiseasesWhichLayout!!)
            binding?.rbInfectiousDiseasesYes -> showView(binding?.etInfectiousDiseasesWhichLayout!!)
            binding?.rbCardiovascularComplaintsNo -> hideView(binding?.etCardiovascularComplaintsWhichLayout!!)
            binding?.rbCardiovascularComplaintsYes -> showView(binding?.etCardiovascularComplaintsWhichLayout!!)
            binding?.rbMetabolicDiseaseNo -> hideView(binding?.etMetabolicDiseaseWhichLayout!!)
            binding?.rbMetabolicDiseaseYes -> showView(binding?.etMetabolicDiseaseWhichLayout!!)
            binding?.rbThyroidDiseasesNo -> hideView(binding?.etThyroidDiseasesWhichLayout!!)
            binding?.rbThyroidDiseasesYes -> showView(binding?.etThyroidDiseasesWhichLayout!!)
            binding?.rbRenalOrLiverDiseasesNo -> hideView(binding?.etRenalOrLiverDiseasesWhichLayout!!)
            binding?.rbRenalOrLiverDiseasesYes -> showView(binding?.etRenalOrLiverDiseasesWhichLayout!!)
            binding?.rbAllergiesNo -> hideView(binding?.etAllergiesWhichLayout!!)
            binding?.rbAllergiesYes -> showView(binding?.etAllergiesWhichLayout!!)
            binding?.rbOtherDiseasesNo -> hideView(binding?.etOtherDiseaseWhichLayout!!)
            binding?.rbOtherDiseasesYes -> showView(binding?.etOtherDiseaseWhichLayout!!)
            binding?.rbPreviousOperationsNo -> hideView(binding?.etPreviousOperationsLayout!!)
            binding?.rbPreviousOperationsYes -> showView(binding?.etPreviousOperationsLayout!!)
            binding?.rbDrugsNo -> hideView(binding?.etDrugsLayout!!)
            binding?.rbDrugsYes -> showView(binding?.etDrugsLayout!!)
        }
    }

    //==============================================================================================
    // Next Activity ...
    private fun nextActivity(){
        binding?.medicalHistoryDate?.text = getCurrentDateTime(1)
        if (isValid()){
            if (binding?.medicalHistorySignaturePad?.isEmpty == false){
                hideView(binding?.btnNext!!)
                val content = binding?.content
                val screenShot = createScreenShot(content!!)
                saveScreenShot(screenShot!!)
                val pdfFile = createPdf(content , screenShot , Files.MEDICAL_PATIENT)
                showMessage(pdfFile.toString())

                showView(binding?.btnNext!!)

                startNewActivity( Activities.SAVING_INFORMATION_ACTIVITY , null , false)
            }else{
                showMessage("Bitte überprüfen Sie, ob Sie unterschrieben haben")
            }
        }
        else{
            showMessage( "Error" )
        }

    }

    //==============================================================================================
    // Additional ...
    private fun isValid(): Boolean {
        val validation = Validation()
        validation.addEmptyValidator(
            binding?.etFirstNameLayout,
            binding?.etLastNameLayout,
            binding?.etBirthdayLayout,
            binding?.etStreetLayout,
            binding?.etHouseNumberLayout,
            binding?.etPostalCodeLayout,
            binding?.etCityLayout
        )
        validation.addRadioGroupValidator(
            binding?.rgInsuranceType,
            binding?.rgToBeEntitled,
            binding?.rgPregnancyQuestion,
            binding?.rgInfectiousDiseases,
            binding?.rgCardiovascularComplaints,
            binding?.rgMetabolicDisease,
            binding?.rgThyroidDiseases,
            binding?.rgRenalOrLiverDiseases,
            binding?.rgAllergies,
            binding?.rgMRSAHospitalGerms,
            binding?.rgCreutzfeldtJakobDisease,
            binding?.rgVitaminB12Deficiency,
            binding?.rgOtherDiseases,
            binding?.rgOtherDiseases,
            binding?.rgPreviousOperations,
            binding?.rgDrugs,
            binding?.rgBleedingGumsQuestion,
            binding?.rgGumTreatmentQuestion,
            binding?.rgJawJointComplaintsQuestion,
            binding?.rgSnoringQuestion,
            binding?.rgCrunchingQuestion,
            binding?.rgInformingOfTheFollowUpAppointmentQuestion
        )
        return validation.isValid
    }
    private fun dialogFinishData(){
        val builder = AlertDialog.Builder(this)
        val viewGroup = findViewById<ViewGroup>(android.R.id.content)
        val dialogView: View =
            LayoutInflater.from(this ).inflate(R.layout.dialog_text, viewGroup, false)
        builder.setView(dialogView)
        val alertDialog = builder.create()
        alertDialog.show()
    }
}