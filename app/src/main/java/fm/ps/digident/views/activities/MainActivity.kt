package fm.ps.digident.views.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.pdf.PdfDocument
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.github.barteksc.pdfviewer.PDFView
import fm.ps.digident.R
import fm.ps.digident.databinding.ActivityMainBinding
import fm.ps.digident.keys.Files
import java.io.File
import java.io.FileOutputStream

class MainActivity : BaseActivity() , View.OnClickListener{

    //==============================================================================================
    // Binding ...
    private var binding: ActivityMainBinding? = null

    //==============================================================================================
    // On Create Activity ...
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initializeActivity()
    }

    //==============================================================================================
    // Initialize Activity ...
    private fun initializeActivity() {
        listener()
    }

    //==============================================================================================
    // Listener Views In Activity ...
    private fun listener(){
        binding?.mainMedicalHistory?.setOnClickListener( this )
        binding?.mainSavingInformation?.setOnClickListener( this )
    }

    override fun onClick(view: View?) {
        when (view) {
            binding?.mainMedicalHistory -> {
                initializePDFView(Files.MEDICAL_PATIENT)
            }
            binding?.mainSavingInformation -> {
                initializePDFView(Files.SAVING_INFORMATION)
            }
        }
    }

    //==============================================================================================
    // Initialize Web View ...
    @SuppressLint("SetJavaScriptEnabled")
    private fun initializePDFView( path: String){

        val applicationFolder = "Digident"
        val applicationDir = File(getExternalFilesDir(null), applicationFolder)
        if (!applicationDir.exists()) {
            applicationDir.mkdir()
        }
        val pdfFile = File(applicationDir, "$path.pdf")

        val pdfView = binding?.mainPdfView
        pdfView?.useBestQuality(true)
        pdfView?.fromFile(pdfFile)?.load()
    }

}