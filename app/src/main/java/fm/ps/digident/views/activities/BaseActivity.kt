package fm.ps.digident.views.activities

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.FragmentManager
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.pdf.PdfDocument
import android.graphics.pdf.PdfDocument.PageInfo
import android.net.ConnectivityManager
import android.os.Build
import android.os.Environment
import android.text.ClipboardManager
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import fm.ps.digident.R
import java.io.File
import java.io.FileOutputStream
import java.util.regex.Pattern
import android.graphics.Bitmap
import android.util.Log
import fm.ps.digident.keys.DateTime
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


open class BaseActivity : AppCompatActivity() {

    protected var transaction: FragmentTransaction? = null
    protected var fm: FragmentManager? = null
    protected var currentFragment = ""
    var drawable: Drawable? = null
    var bitmap: Bitmap? = null

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
        //        AppLanguageUtil.getInstance().setApplicationLanguage(this, AppLanguageUtil.getInstance().getAppLanguage());
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // TODO Auto-generated method stub
        val id = item.itemId
        if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkAttachedImage(view: ImageView): Boolean {
        val drawable = view.drawable
        var hasImage = drawable != null
        if (hasImage && drawable is BitmapDrawable) {
            hasImage = drawable.bitmap != null
        }
        return hasImage
    }

    fun restart() {
        val intent = intent
        finish()
        startActivity(intent)
    }

    fun startNewActivity(newActivity: String , extras: Intent?, clearStack: Boolean) {
        val intent = Intent("fm.ps.digident")
        intent.addCategory(newActivity)

        if (clearStack) {
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        if (extras != null) {
            intent.putExtras(extras)
        }
        hideKeyboard()
        startActivity(intent)
        if (clearStack) {
            finish()
        }
        //        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    fun replaceFragment(fragment: Fragment?, tag: String, container: Int, addToBackStack: Boolean) {
        currentFragment = tag
        val manager = supportFragmentManager
        transaction = manager.beginTransaction()
        if (!addToBackStack) {
            transaction!!.replace(container, fragment!!, tag)
        } else {
            transaction!!.replace(container, fragment!!, tag)
            transaction!!.addToBackStack(tag)
        }
        transaction!!.commit()
    }

    fun replaceFragment(container: Int, fragment: Fragment?, tag: String) {
        replaceFragment(fragment, tag, container, false)
    }

    //==============================================================================================
    // Convert ...
    private fun convertColorToDrawableColor(color: Int): ColorDrawable {
        return ColorDrawable(ContextCompat.getColor(this, color))
    }

    fun createScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) {
            bgDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.WHITE)
        }
        view.draw(canvas)
        return bitmap
    }

    open fun saveScreenShot(bitmap: Bitmap) {
        val fileName = "screenShot.png"
        val dirPath = Environment.getExternalStorageDirectory().absolutePath + "/Screenshots"
        val dir = File(dirPath)
        if (!dir.exists()) dir.mkdirs()
        val file = File(dirPath, fileName)
        try {
            val fOut = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut)
            fOut.flush()
            fOut.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        showMessage(dirPath)
    }

    open fun drawableToBitmap(drawable: Drawable): Bitmap? {
        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }
        val bitmap =
            Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

     fun createPdf(view: View , bitmap: Bitmap , name: String): File {
        val applicationFolder = "Digident"
        val pfdWidth = view.width
        val pdfHeight = view.height
        val document = PdfDocument()
        val pageInfo = PageInfo.Builder(pfdWidth, pdfHeight, 1).create()
        val page = document.startPage(pageInfo)
        val canvas = page.canvas
        canvas.drawBitmap(bitmap, 0f, 50f, null)
        document.finishPage(page)
        val applicationDir = File(getExternalFilesDir(null), applicationFolder)
        if (!applicationDir.exists()) {
            applicationDir.mkdir()
        }
        val pdfFile = File(applicationDir, "$name.pdf")
        try {
            val outputStream = FileOutputStream(pdfFile)
            document.writeTo(outputStream)
            outputStream.flush()
            outputStream.close()
        } catch (e: java.lang.Exception) {
            Toast.makeText(this, "Something wrong: $e", Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }

        // close the document
        document.close()
        //Toast.makeText(this, "PDF is created!!!", Toast.LENGTH_SHORT).show();
        return pdfFile
    }
    //==============================================================================================
    // Key Board ...
    private var inputMethodManager: InputMethodManager? = null
    val isKeyboardVisible: Boolean
        get() {
            inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            return inputMethodManager!!.isAcceptingText
        }

    fun hideKeyboard() {
        try {
            inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            if (inputMethodManager != null) {
                inputMethodManager!!.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            }
        } catch (e: Exception) {
        }
    }

    //----------------------------------------------------------------------------------------------
    fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun hideStatusBar() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    fun setStatusBarTransparent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = window
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        }
    }

    fun hideNavigationBar() {
        val decorView = window.decorView
        val uiOptions = (View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
        decorView.systemUiVisibility = uiOptions
    }

    fun changeStatusBarColor(color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.statusBarColor = resources.getColor(color, this.theme)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(color)
        }
    }

    fun changeStatusBarIconsColor(shouldChangeStatusBarTintToDark: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val decor = window.decorView
            if (shouldChangeStatusBarTintToDark) {
                decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                // We want to change tint color to white again.
                // You can also record the flags in advance so that you can turn UI back completely if
                // you have set other flags before, such as translucent or full screen.
                decor.systemUiVisibility = 0
            }
        }
    }

    fun showOrHideView(view:View) {
        view.visibility = if (view.visibility == View.VISIBLE){
            View.GONE
        } else{
            View.VISIBLE
        }
    }

    fun enabledView(view:View) {
        view.isEnabled = view.isEnabled != true
    }

    fun enableActionBarHomeButton(colorId: Int) {
        val upArrow = ContextCompat.getDrawable(this, R.drawable.abc_ic_ab_back_material)
        upArrow!!.setColorFilter(ContextCompat.getColor(this, colorId), PorterDuff.Mode.SRC_ATOP)
        val actionBar = supportActionBar
        actionBar!!.setHomeAsUpIndicator(upArrow)
        actionBar.setHomeButtonEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }// connected to wifi

    // connected to the internet
    val isConnectedToInternet: Boolean
        get() {
            val connManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = connManager.activeNetworkInfo
            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.type == ConnectivityManager.TYPE_WIFI ||
                    activeNetwork.type == ConnectivityManager.TYPE_MOBILE
                ) {
                    // connected to wifi
                    return true
                }
            }
            return false
        }

    fun copyToClipboard(text: String?) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.text = text
        } else {
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = ClipData.newPlainText("Copied Text", text)
            clipboard.setPrimaryClip(clip)
        }
        Toast.makeText(this, "تم نسخ النص إلى الحافضة", Toast.LENGTH_SHORT).show()
    }

    //==============================================================================================
    // Animation ...
    //    public Animation FadeInAnim(){
    //        return AppController.getInstance().getAnimationController().getFadeIn();
    //    }
    //    public Animation FadeOutAnim(){
    //        return AppController.getInstance().getAnimationController().getFadeOut();
    //    }
    //    public Animation BlinkAnim(){
    //        return AppController.getInstance().getAnimationController().getBlink();
    //    }
    //    public Animation SlideUpAnim(){
    //        return AppController.getInstance().getAnimationController().getSlideUp();
    //    }
    //    public Animation SlideDownAnim(){
    //        return AppController.getInstance().getAnimationController().getSlideDown();
    //    }
    //    public void startAnimation(View view , Animation animation){
    //        view.startAnimation(animation);
    //    }
    //
    //    public void endAnimation(View view){
    //        view.getAnimation().cancel();
    //        view.clearAnimation();
    //    }
    //==============================================================================================
    // View ...
    //    public void HideView(OnListenerHideView onListenerHideView , final View view , Animation animation ){
    //        AppController.getInstance().getBaseController().getHideView(onListenerHideView).HideView(view,animation);
    //    }
    //    public void HideView(OnListenerHideView onListenerHideView , final View view ){
    //        AppController.getInstance().getBaseController().getHideView(onListenerHideView).HideView(view);
    //    }
    //    public void ShowView (OnListenerShowView onListenerShowView , final View view , Animation animation ){
    //        AppController.getInstance().getBaseController().getShowView(onListenerShowView).ShowView(view,animation);
    //    }

    fun hideView(view: View) {
        view.visibility = View.GONE
    }

    fun showView(view: View) {
        view.visibility = View.VISIBLE
    }

    //==============================================================================================
    // Dialog ...
    //    public DialogController dialogController(){
    //        return AppController.getInstance().getDialogController();
    //    }
    //
    //    public void ShowDialog(String title  , String desc , int handler){
    //        dialogController().getTitleDescriptionDialog(this,title,desc,handler);
    //    }
    //==============================================================================================
    // Context Application ...
    //    public Context contextApplication(){
    //        return AppController.getInstance().getContext();
    //    }
    //==============================================================================================
    // Firebase ...
    //    public CheckIfTheDataIsPresentOnFirebase checkIfTheDataIsPresentOnFirebase(OnListenerCheckDataIsPresent onListenerCheckDataIsPresent){
    //        return AppController.getInstance().getFireBaseController().getCheckIfTheDataIsPresentOnFirebase(onListenerCheckDataIsPresent);
    //    }
    //    public FireBaseController fireBaseController(){
    //        return AppController.getInstance().getFireBaseController();
    //    }
    //==============================================================================================
    // Preference ...
    //    public UserPreferences userPreferences(){
    //        return AppController.getInstance().getPreferenceController().getUserPreferences();
    //    }
    //==============================================================================================
    // Ads ...
    //    public NativeAds getNativeAds(){
    //        return AppController.getInstance().getAdsController().getNativeAds();
    //    }
    //==============================================================================================
    // media ...
    //    public MediaController mediaController(){
    //        return AppController.getInstance().getMediaController();
    //    }
    //==============================================================================================
    // Share ...
    fun shareText(text: String?) {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_SUBJECT, "تطبيق شهية")
        i.putExtra(Intent.EXTRA_TEXT, text)
        startActivity(Intent.createChooser(i, "مشاركة النص عبر"))
    }
    fun validateEmail(email: String?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun validatePassword(password: String?): Boolean {
        val PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+])(?=\\\\S+$).{8,20}$"
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    fun isContainString(source: String?, subItem: String): Boolean {
        val pattern = "\\b$subItem\\b"
        val p = Pattern.compile(pattern)
        val m = p.matcher(source)
        return m.find()
    }


    //==============================================================================================
    // Date And Time Zone ...
    fun getCurrentDateTime(type: Int): String? {
        var dateFormatter: DateFormat? = null
        when (type) {
            DateTime.TYPE_DATE_AND_TIME -> {
                dateFormatter = SimpleDateFormat("yyyy/MM/dd hh:mm")
            }
            DateTime.TYPE_DATE -> {
                dateFormatter = SimpleDateFormat("yyyy/MM/dd")
            }
            DateTime.TYPE_TIME_24_HOURS -> {
                dateFormatter = SimpleDateFormat("hh:mm a")
            }
            DateTime.TYPE_TIME_12_HOURS -> {
                dateFormatter = SimpleDateFormat("kk:mm a")
            }
        }
        dateFormatter!!.isLenient = false
        val today = Date()
        return dateFormatter.format(today)
    }

    fun convertToTimeZone(currentDate: String?): String? {
        try {
            val sdfIn = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            sdfIn.timeZone = Calendar.getInstance().timeZone
            sdfIn.timeZone = TimeZone.getTimeZone("UTC")
            val date = sdfIn.parse(currentDate)
            val sdfOut = SimpleDateFormat("kk:mm a")
            sdfOut.timeZone = TimeZone.getDefault()
            Log.d("TIME_ZONE", "onCreate: " + sdfOut.format(date))
            return sdfOut.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ""
    }
    //==============================================================================================
    // Message ...
    fun showMessage(message: String) {
        Toast.makeText( this , message , Toast.LENGTH_LONG).show()
    }
}