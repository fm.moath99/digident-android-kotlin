package fm.ps.digident.validator

import com.google.android.material.textfield.TextInputLayout
import android.widget.EditText

abstract class TextInputLayoutValidator(
    private val errorMessageId: Int,
    vararg textInputLayouts: TextInputLayout
) {

    private var textInputLayouts: Array<TextInputLayout> = textInputLayouts as Array<TextInputLayout>

    abstract fun isValid(editText: EditText?): Boolean

    val isValid: Boolean
        get() {
            var isValid = true
            for (textInputLayout in textInputLayouts) {
                if (!isValid(textInputLayout.editText)) {
                    textInputLayout.error = textInputLayout.context.getText(errorMessageId)
                    isValid = false
                } else {
                    textInputLayout.error = null
                }
            }
            return isValid
        }

}