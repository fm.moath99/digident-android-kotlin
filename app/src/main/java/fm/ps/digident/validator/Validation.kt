package fm.ps.digident.validator

import com.google.android.material.textfield.TextInputLayout
import android.widget.RadioGroup
import java.util.ArrayList

class Validation {

    private val validators = ArrayList<Validator>()

    fun addEmptyValidator(vararg textInputLayouts: TextInputLayout?) {
        for (textInputLayout in textInputLayouts) {
            validators.add(EmptyValidator(textInputLayout!!))
        }
    }

    fun addRadioGroupValidator(vararg radioGroups: RadioGroup?) {
        for (radioGroup in radioGroups) {
            validators.add(RadioGroupValidator(radioGroup!!))
        }
    }

    val isValid: Boolean
        get() {
            var isValid = true
            for (validator in validators) {
                if (!validator.isValid) {
                    if (isValid) {
                        validator.requestFocus()
                    }
                    isValid = false
                }
            }
            return isValid
        }
}