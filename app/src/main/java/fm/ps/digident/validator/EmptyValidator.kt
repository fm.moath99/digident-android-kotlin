package fm.ps.digident.validator

import com.google.android.material.textfield.TextInputLayout
import fm.ps.digident.R
import fm.ps.digident.util.EditTextUtil

class EmptyValidator(private var textInputLayout: TextInputLayout) : Validator(textInputLayout) {

    override val isValid: Boolean
        get() {
            if (EditTextUtil.isEmpty(textInputLayout)) {
                textInputLayout.isErrorEnabled = true
                textInputLayout.error = getErrorMessage(textInputLayout)
                return false
            }
            textInputLayout.error = null
            textInputLayout.isErrorEnabled = false
            return true
        }

    companion object {
        private const val ERROR_MESSAGE_ID: Int = R.string.error_empty

        private fun getErrorMessage(textInputLayout: TextInputLayout): String {
            return textInputLayout.context
                .getString(ERROR_MESSAGE_ID, textInputLayout.hint)
        }
    }

}