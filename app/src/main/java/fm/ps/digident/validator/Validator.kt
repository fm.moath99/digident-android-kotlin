package fm.ps.digident.validator

import android.view.View

abstract class Validator(var view: View) {

    abstract val isValid: Boolean

    fun requestFocus() {
        view.requestFocus()
    }
}