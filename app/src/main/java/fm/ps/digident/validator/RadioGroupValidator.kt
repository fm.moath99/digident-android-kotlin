package fm.ps.digident.validator

import android.widget.RadioGroup
import fm.ps.digident.R


class RadioGroupValidator(private var radioGroup: RadioGroup) : Validator(radioGroup) {

    override val isValid: Boolean
        get() {
            if (radioGroup.checkedRadioButtonId == -1) {
                radioGroup.setBackgroundResource(R.drawable.radio_group_error_border)
                return false
            }
            radioGroup.setBackgroundResource(0)
            return true
        }

}