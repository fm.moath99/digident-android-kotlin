package fm.ps.digident.validator

import android.view.View

class NameValidator(view: View?) : Validator(
    view!!
) {
    override val isValid: Boolean
        get() = false

    companion object {
        private const val NAME_PATTERN = "^[a-z0-9_-]{3,15}$"
    }
}