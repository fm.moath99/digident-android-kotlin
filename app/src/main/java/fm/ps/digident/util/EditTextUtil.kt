package fm.ps.digident.util

import android.text.InputFilter
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import fm.ps.digident.validator.TextInputLayoutValidator

object EditTextUtil {

    fun isEmpty(editText: EditText?): Boolean {
        if (editText == null) {
            return true
        }
        val str: CharSequence = editText.getText().toString()
        return TextUtils.isEmpty(str)
    }

    fun isEmpty(textInputLayout: TextInputLayout?): Boolean {
        return if (textInputLayout == null) {
            true
        } else isEmpty(textInputLayout.getEditText())
    }

    fun getText(editText: EditText?): String {
        return if (editText == null) "" else editText.getText().toString().trim { it <= ' ' }
    }

    fun isInValid(vararg textInputLayoutValidators: TextInputLayoutValidator): Boolean {
        var isInValid = false
        for (textInputLayoutValidator in textInputLayoutValidators) {
            if (!textInputLayoutValidator.isValid) {
                isInValid = true
            }
        }
        return isInValid
    }

    fun addFocusChangeListener(vararg textInputLayouts: TextInputLayout) {
        for (textInputLayout in textInputLayouts) {
            val editText: EditText? = textInputLayout.editText
            editText?.filters = arrayOf<InputFilter>(InputFilter.AllCaps())
            editText?.setOnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    val text: String = editText.text.toString()
                    if (!text.isEmpty()) {
                        editText.setText(
                            text.substring(0, 1).toUpperCase() + text.substring(1)
                        )
                    }
                }
            }
        }
    }
}