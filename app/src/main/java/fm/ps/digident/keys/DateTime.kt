package fm.ps.digident.keys

import java.text.SimpleDateFormat

object DateTime {
    const val TYPE_DATE_AND_TIME = 1
    const val TYPE_DATE = 2
    const val TYPE_TIME_24_HOURS = 3
    const val TYPE_TIME_12_HOURS = 4
}