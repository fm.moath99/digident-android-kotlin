package fm.ps.digident.keys

object Activities {

    const val SPLASH_ACTIVITY = "SplashActivity"
    const val MAIN_ACTIVITY = "MainActivity"
    const val SELECT_PATIENT_CONDITION_ACTIVITY = "SelectPatientConditionActivity"
    const val LOGIN_ACTIVITY = "LoginActivity"
    const val MEDICAL_HISTORY_ACTIVITY = "MedicalHistoryActivity"
    const val SAVING_INFORMATION_ACTIVITY = "SavingInformationActivity"

}